// var formElem = document.getElementById('formElem');
// formElem.onsubmit = async (e) => {
//     e.preventDefault();

//     let response = await fetch('/hook/', {
//       method: 'POST',
//       data: new FormData(formElem),
//       headers: { "Content-Type": "multipart/form-data" }
//     });

//     let result = await response;

//     console.log(result);
//   };

var testForm = document.getElementById("formElem");
testForm.onsubmit = function (event) {
  event.preventDefault();

  var request = new XMLHttpRequest();

  // POST to httpbin which returns the POST data as JSON
  request.open("POST", "http://localhost:9000/", true);

  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  var formData2 = document.getElementById("pull");
  console.log(formData2.value);

  var formData = new FormData(document.getElementById("formElem"));
  formData.append("name", "hurz");


  request.send(`${formData2.name}=${formData2.value}`);

  request.onload = function () {
        if (request.readyState === request.DONE) {
            if (request.status === 200) {
                var done = document.getElementById('done');
                done.innerHTML = request.response;
                console.log(request.response);
                console.log(request.responseText);
            }
        }
    };
};
