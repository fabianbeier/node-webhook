const express = require('express');
const app = express();
const port = 9000;
const bodyParser = require('body-parser');
const { exec } = require('child_process');
var cors = require('cors');

app.use(cors());
// const router = express.Router();
// let NodeMonkey = require("node-monkey")
// NodeMonkey()
var path = require('path');



app.use(bodyParser.urlencoded({ extended: true }));

 app.use(bodyParser.json());
app.post('/', async function (request, response) {
    const body = request;

    console.log(body.body);
    const result = await new Promise((resolve, reject) => {

        exec(`sh script2.sh ${body.body.pull}`,
       (error, stdout, stderr) => {
        console.log(stdout);
        console.log(stderr);
        // response.send(stdout);
           if (error !== null) {
            reject(error);
               console.log(`exec error: ${error}`);
           } else {
             resolve(stdout);
           }

       });
    });

    response.send(result);
    response.status(200).end();
});

app.use(express.static(__dirname));

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname + '/index.html'));
  console.log('hallo welt');
});

app.listen(port, () => {
    console.log(`Express api/webhook app listening at http://localhost:${port}`);
});

