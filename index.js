const express = require('express');
const app = express();
const port = 9000;
const bodyParser = require('body-parser');
const { exec } = require('child_process');
var cors = require('cors');

app.use(cors());
// const router = express.Router();
// let NodeMonkey = require("node-monkey")
// NodeMonkey()
var path = require('path');



app.use(bodyParser.urlencoded({ extended: true }));

 app.use(bodyParser.json());
app.post('/', async function (request, response) {
    const body = request;
    const result = await new Promise((resolve, reject) => {

    console.log(body.body);
    if (body.body.pull.length !== 0) {
        exec(`sh script.sh ${body.body.pull}`,
       (error, stdout, stderr) => {
        console.log(stdout);
        console.log(stderr);
        // response.send(stdout);
           if (error !== null) {
            reject(error);
               console.log(`exec error: ${error}`);
           } else {
            resolve(stdout);
           }
       });
    } else if (body.body.init.length !== 0) {
        exec(`sh init-project.sh ${body.body.init}`,
        (error, stdout, stderr) => {
         console.log(stdout);
         console.log(stderr);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
    } else {
        console.log('Empty')
    }
});
    response.send(result);
    response.status(200).end();
});

app.use(express.static(__dirname));

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname + '/index.html'));
  console.log('hallo welt');
});

app.listen(port, () => {
    console.log(`Express api/webhook app listening at http://localhost:${port}`);
});

